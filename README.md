[![Lighthouse score: 100/100](https://lighthouse-badge.appspot.com/?score=100)](https://github.com/ebidel/lighthouse-badge)

### Hello

This is the source of my [website](https://sivasubramanyam.me).

I initially set this up during my college days(~2013) and much
of the layout and styles haven't changed since then.

This site is built with Flask and I use Flask-Static-Compress
to generate static files from the contents.

I wrote this in Flask as I was deeply involved with the Python
community back then and I used to run several other apps with this.
Much of those apps no longer exist but I have kept the site around
after trimming down the original version of this code.

At one point of time, this site and the associated apps were being
served from AWS and I wrote a [blog](https://sivasubramanyam.me/flask-aws/)
about the setup which you can read if you are interested.

Currently, this is just hosted on Github pages. I have also been
pondering about moving this to some other framework as doing some
basic stuff like lazy-loading, minifying and even adding `defer` and
`async` attributes can now be accomplished only through hacks,
gross violations of DRI or me writing Flask extensions by myself,
none of which I have much time for these days. Hopefully, I would
get around to doing it one day :)

I do a lot of experimentation with this site and some successful
ones have been made as separate projects that you can check out
on my Gitub profile. Some have even made it to my workplace :P
