title: paste-clean-diff
date: June, 2018
description: A VSCode extension to paste Git diff without the plus and minus signs
tags: Tooling
order: 28

A VSCode extension to paste Git diffs without leading plus and minus signs.

Open the Command Palette(Cmd+Shift+P on macOS, Ctrl+Shift+P on Windows and Linux) and type `Paste Clean Diff`.

<div class="ajanta">
  <img
    class="img-responsive center-block pixelated blur"
    src="/static/images/lowres/paste-clean-diff.png" 
    alt="Paste Clean Diff"
    data-image-format="gif">

  <img class="img-responsive center-block original">
</div>

To clean up an already pasted diff, select the diff and then choose `Clean Diff` from the Command Palette.

<div class="ajanta">
  <img
    class="img-responsive center-block pixelated blur"
    src="/static/images/lowres/clean-pasted-diff.png" 
    alt="Clean Pasted Diff"
    data-image-format="gif">

  <img class="img-responsive center-block original">
</div>

You can install it from the VSCode Marketplace [here](https://marketplace.visualstudio.com/items?itemName=sivasubramanyam.paste-clean-diff).
