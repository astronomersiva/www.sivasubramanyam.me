title: niram
date: May, 2018
description: Extract colors from an image
tags: React, Javascript
order: 25

niram(Tamil for *color*) is an app that I built for learning React and CSS-in-JS.

niram lets you upload an image and displays the 5 most common color codes of that image.

<div class="ajanta">
  <img
    class="img-responsive center-block pixelated blur"
    src="/static/images/lowres/niram.png" 
    alt="Screenshot">

  <img class="img-responsive center-block original">
</div>

Head out to the [demo page](https://astronomersiva.github.io/niram/) to try it out and [here is the repo](https://github.com/astronomersiva/niram), if you want to see the code or file an issue :)
