title: Hexa Clock    
date: March, 2015
description: A simple Javascript clock that changes its background color with each passing second.  
tags: Javascript  
order: 10

A small fun project using Javascript that changes the document's background color every second by converting the current time to color codes.

<object data="../static/timecolor/timeColor.html" width="600" height="400"><embed src="../static/timecolor/timeColor.html" width="600" height="400">Error: Embedded data could not be displayed.</object>

[[Source on Github]](http://github.com/astronomersiva/timeColor/)
