title: Weather Forecast  
date: October, 2014
description: A Javascript based webapp to fetch your location using the W3C Geolocation API and display relevant weather data.
tags: Javascript  
order: 8

I have recently decided to do one fun project every weekend. This weekend, I decided to create a site that would fetch the visitor's location and display the weather forecast for that location.

I used the W3C Geolocation API for fetching the location and OpenWeather Map's API for fetching weather forecasts. I used an [awesome weather icon pack](http://erikflowers.github.io/weather-icons/) by Erik Flowers for the icons. If you just want to see it working, then head [here](http://weather.sivasubramanyam.me). If you want to see the code, then continue reading. First the simple HTML markup.

The real work though, is done by the following Javascript code.

[[Source on Github]](http://github.com/astronomersiva/weather/)
